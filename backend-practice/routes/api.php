<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * route /api/v1
 */
Route::group(['prefix' => 'v1'],function () {

    Route::post('/auth/login', [\App\Http\Controllers\AuthController::class, 'login']);
    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::post('/auth/logout', [\App\Http\Controllers\AuthController::class, 'logout']);

        /**
         * route api/v1/cat-vehicle-type/list-by-select
         *
         * Devuelve listado de tipos de vehículos
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse    Respuesta en formato JSON
         * @response
         */
        Route::get('/cat-vehicle-type/list-by-select', [
            \App\Http\Controllers\CatVehicleTypeController::class, 'listBySelected',
        ]);

        /**
         * route /v1/cat-vehicle-brand/list-by-select
         *
         * Devuelve listado de marcas para vehículos
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse    Respuesta en formato JSON
         * @response
         */
        Route::get('/cat-vehicle-brand/list-by-select', [
            \App\Http\Controllers\CatVehicleBrandController::class, 'listBySelected',
        ]);

        /**
         * route /v1/cat-vehicle/list
         *
         * Devuelve listado de vehículos
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse    Respuesta en formato JSON
         * @response
         */
        Route::get('/cat-vehicle/list', [
            \App\Http\Controllers\CatVehicleController::class, 'listt',
        ]);

        Route::post('/cat-vehicle/save', [
            \App\Http\Controllers\CatVehicleController::class, 'store',
        ]);
    });

});
