<?php

namespace App\Models;

use App\Scopes\MotorcycleScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CatVehicleMotorcycle extends CatVehicle
{
    use HasFactory;

    protected static function booted()
    {
        static::addGlobalScope(new MotorcycleScope);
    }


}
