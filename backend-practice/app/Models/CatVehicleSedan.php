<?php

namespace App\Models;

use App\Scopes\MotorcycleScope;
use App\Scopes\SedanScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CatVehicleSedan extends CatVehicle
{
    use HasFactory;

    protected static function booted()
    {
        static::addGlobalScope(new SedanScope);
    }


}
