<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatVehicle extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cat_vehicle';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'det_description' => 'string',
        'engine' => 'string',
        'serie' => 'string',
        'hp' => 'integer',
        'cat_vehicle_type_id' => 'integer',
        'cat_vehicle_brand_id' => 'integer',
        'active' => 'boolean',
    ];
}
