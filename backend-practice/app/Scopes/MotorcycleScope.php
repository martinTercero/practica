<?php


namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MotorcycleScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('cat_vehicle_type_id', '=', 2);
    }
}
