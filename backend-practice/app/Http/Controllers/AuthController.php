<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Validator;

class AuthController
{

    public function login(Request $request)
    {
        Log::info("Controller: AuthController, Method: login");
        $attr = Validator::make($request->all(), [
            'email' => 'required|string|email|',
            'password' => 'required|string|min:3'
        ]);

        if ($attr->fails()) {
            return response()->json(
                [
                    "message" =>
                        ["title" => env("TITLE_RESPONSE_JON"), 'severity' => "success", 'text' => 'Ingresa usuario y contraseña.'],
                    'data' => $attr->errors(),
                    "response" => false
                ], 200);
        }

        try {
            if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return response()->json(
                    [
                        "message" =>
                            ["title" => env("TITLE_RESPONSE_JON"), 'severity' => "success", 'text' => 'Usario o contraseña incorrectos.'],
                        'data' => [],
                        "response" => false
                    ], 200);
            }

            $user= auth()->user();
            $user["token_access"]= auth()->user()->createToken('API Token')->plainTextToken;

            return response()->json(
                [
                    "message" =>
                        ["title" => 'Success', 'severity' => "success", 'text' => 'Sesión iniciada.'],
                    'user' => auth()->user(),
                    "response" => true
                ], 200);
        } catch (\Exception $e) {
            return response()->json(
                [
                    "message" =>
                        ["title" => 'Warning', 'severity' => "success", 'text' => 'Hubo un eror al iniciar sesión.'.$e],
                    'token' => "",
                    'user' => "",
                    "response" => false
                ], 401);
        }
    }

    public function logout (Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();
            return response()->json(
                [
                    "message" =>
                        ["title" => 'Success', 'severity' => "success", 'text' => 'Sesión cerrada.'],
                    'token' => null,
                    'user' => null,
                    "response" => true
                ], 200);
        } catch (\Exception $e) {
            return response()->json(
                [
                    "message" =>
                        ["title" => 'Warning', 'severity' => "success", 'text' => 'Hubo un eror al cerrar sesión.'],
                    'token' => "",
                    'user' => "",
                    "response" => false
                ], 500);
        }
    }
}
