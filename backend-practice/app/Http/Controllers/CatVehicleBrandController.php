<?php

namespace App\Http\Controllers;

use App\Models\CatVehicleBrand;
use DB;
use Illuminate\Http\Request;

class CatVehicleBrandController extends Controller
{

    /**
     * Función que devuelve listado de marcas para vehículos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listBySelected(Request $request){
        try {
            $list = CatVehicleBrand::where('active','=',true)
                ->select('cat_vehicle_brand.*', DB::raw('CONCAT_WS(" ", code ,name) as name_full'))
                ->get();
            return response()->json(["message"=>[
                    "title"=>"Success",
                    'severity'=>"success",
                    'text'=>'Data obtained successfully.'],'data'=>$list,"response"=>true]
                ,200);
        }catch (\Exception $e) {
            return response()->json(["message"=>[
                    "title"=>"Warning",
                    'severity'=>"warning",
                    'text'=>'Error, try again.'],'data'=>null,"response"=>false]
                ,200);
        }

    }
}
