<?php

namespace App\Http\Controllers;

use App\Models\CatVehicleType;
use DB;
use Illuminate\Http\Request;

class CatVehicleTypeController extends Controller
{

    /**
     * Función que devuelve listado de tipos de vehículos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listBySelected(Request $request){
        try {
            $list = CatVehicleType::where('active','=',true)
                ->select('id', DB::raw('CONCAT_WS(" ", description,num_tires, "Llantas") as description_full'))
                ->get();
            return response()->json(["message"=>[
                    "title"=>"Success",
                    'severity'=>"success",
                    'text'=>'Data obtained successfully.'],'data'=>$list,"response"=>true]
                ,200);
        }catch (\Exception $e) {
            return response()->json(["message"=>[
                    "title"=>"Warning",
                    'severity'=>"warning",
                    'text'=>'Error, try again.'],'data'=>null,"response"=>false]
                ,200);
        }
    }

}
