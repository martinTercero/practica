<?php

namespace App\Http\Controllers;

use App\Models\CatVehicle;
use App\Models\CatVehicleMotorcycle;
use App\Models\CatVehicleSedan;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;

class CatVehicleController extends Controller
{

    private function _vehicleList(Request $request) {
        $listQuery = CatVehicle::select('cat_vehicle.*',
            DB::raw("(select name from cat_vehicle_brand where  cat_vehicle_brand.id=cat_vehicle.cat_vehicle_brand_id) as brand"),
            DB::raw("(select description from cat_vehicle_type where  cat_vehicle_type.id=cat_vehicle.cat_vehicle_type_id) as type"),
            DB::raw('(CASE cat_vehicle.active WHEN true THEN "Activo" ELSE  "Baja" END) as active_f')
        );
        switch ($request->filter) {
            case "Motocicleta":
                $listQuery = CatVehicleMotorcycle::select('cat_vehicle.*',
                    DB::raw("(select name from cat_vehicle_brand where  cat_vehicle_brand.id=cat_vehicle.cat_vehicle_brand_id) as brand"),
                    DB::raw("(select description from cat_vehicle_type where  cat_vehicle_type.id=cat_vehicle.cat_vehicle_type_id) as type"),
                    DB::raw('(CASE cat_vehicle.active WHEN true THEN "Activo" ELSE  "Baja" END) as active_f'));
                break;
            case "Sedan":
                $listQuery = CatVehicleSedan::select('cat_vehicle.*',
                    DB::raw("(select name from cat_vehicle_brand where  cat_vehicle_brand.id=cat_vehicle.cat_vehicle_brand_id) as brand"),
                    DB::raw("(select description from cat_vehicle_type where  cat_vehicle_type.id=cat_vehicle.cat_vehicle_type_id) as type"),
                    DB::raw('(CASE cat_vehicle.active WHEN true THEN "Activo" ELSE  "Baja" END) as active_f'));
        }
        return $listQuery->where('active','=',true)->get();
    }

    /**
     * Función que devuelve el listado de vehículos en base de datos.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listt(Request $request) {
        try {
            return response()->json(["message"=>[
                    "title"=>"Success",
                    'severity'=>"success",
                    'text'=>'Data obtained successfully.'],'data'=>$this->_vehicleList($request),"response"=>true]
                ,200);
        }catch (\Exception $e) {
            return response()->json(["message"=>[
                    "title"=>"Warning",
                    'severity'=>"warning",
                    'text'=>'Error, try again.'.$e],'data'=>null,"response"=>false]
                ,200);
        }
    }


    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'code' => 'required|max:10',
            'name' => 'required',
            'engine' => 'required',
            'serie' => 'required',
            'hp' => 'required|numeric',
            'year_model' => 'required|numeric',
            'type' => 'required',
            'brand' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(["message"=>[
                    "title"=>"Warning",
                    'severity'=>"warning",
                    'text'=>$validator->errors()],'data'=>null,"response"=>false]
                ,200);
        }

        DB::beginTransaction();
        try {
            $catVehicle = new CatVehicle;
            $catVehicle->code=$request->code;
            $catVehicle->name=$request->name;
            $catVehicle->det_description=$request->det_description;
            $catVehicle->engine=$request->engine;
            $catVehicle->serie=$request->serie;
            $catVehicle->hp=$request->hp;
            $catVehicle->year_model=$request->year_model;
            $catVehicle->cat_vehicle_type_id=$request->type;
            $catVehicle->cat_vehicle_brand_id=$request->brand;
            $catVehicle->save();
            DB::commit();
            return response()->json(["message"=>[
                    "title"=>"Success",
                    'severity'=>"success",
                    'text'=>'Record saved.'],'data'=>$this->_vehicleList($request),"response"=>true]
                ,200);
        }catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return response()->json(["message"=>[
                    "title"=>"Warning",
                    'severity'=>"warning",
                    'text'=>'Error saving record, try again.'],'data'=>null,"response"=>false]
                ,200);
        }
    }
}
