<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_vehicle', function (Blueprint $table) {
            $table->id();
            $table->string('code',20)->index();
            $table->string('name',150);
            $table->text('det_description')->nullable();
            $table->string('engine',150)->nullable();
            $table->string('serie',50)->nullable();
            $table->double('hp');
            $table->integer('year_model');
            $table->foreignId('cat_vehicle_type_id')->constrained('cat_vehicle_type');
            $table->foreignId('cat_vehicle_brand_id')->constrained('cat_vehicle_brand');
            $table->boolean("active")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_vehicles');
    }
}
