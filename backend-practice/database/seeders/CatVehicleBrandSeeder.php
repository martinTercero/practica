<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class CatVehicleBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_vehicle_brand')->insert([
            'code' => 'VW',
            'name' => 'Volkswagen',
            'logo' => '/images/vw.png',
            'active' => true,
            'created_at' => date('Y-m-d h:i')
        ]);
        DB::table('cat_vehicle_brand')->insert([
            'code' => 'DU',
            'name' => 'Ducati',
            'logo' => '/images/ducati.png',
            'active' => true,
            'created_at' => date('Y-m-d h:i')
        ]);
    }
}
