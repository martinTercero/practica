<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class CatVehicleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('cat_vehicle_type')->insert([
            'id' => 1,
            'description' => 'Sedan',
            'num_tires' => '4',
            'active' => true,
            'created_at' => date('Y-m-d h:i')
        ]);
        DB::table('cat_vehicle_type')->insert([
            'id' => 2,
            'description' => 'Motocicleta',
            'num_tires' => '2',
            'active' => true,
            'created_at' => date('Y-m-d h:i')
        ]);
    }
}
