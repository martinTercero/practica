<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'name' => 'Administrator',
            'email' => 'martin.galdino@gmail.com',
            'password' => bcrypt('secret'),
            'created_at' => date('Y-m-d h:i')
        ]);
    }
}
