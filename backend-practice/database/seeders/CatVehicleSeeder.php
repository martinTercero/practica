<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class CatVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_vehicle')->insert([
            'code' => 'VW001',
            'name' => 'VW Jetta',
            'det_description' => 'Lorem ipsum dolor',
            'engine' => 'XXXXXXXX-Y',
            'serie' => 'zxcvbnm',
            'hp' => 150,
            'year_model' => 2022,
            'cat_vehicle_type_id' => 1,
            'cat_vehicle_brand_id' => 1,
            'active' => true,
            'created_at' => date('Y-m-d h:i')
        ]);

        DB::table('cat_vehicle')->insert([
            'code' => 'DU002',
            'name' => 'DUCATI Monster',
            'det_description' => 'Lorem ipsum dolor 2',
            'engine' => '937-Testastretta',
            'serie' => 'qwertyuiop',
            'hp' => 50,
            "year_model"=>2022,
            'cat_vehicle_type_id' => 2,
            'cat_vehicle_brand_id' => 2,
            'active' => true,
            'created_at' => date('Y-m-d h:i')
        ]);
    }
}
