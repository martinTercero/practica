import axios from "../../common/axiosInstance";
import {loginFailure, loginStart, loginSuccess, logoutSuccess} from "./authActions";
import {toast} from "react-toastify";
import {configHeaderJsonToken} from "./AuthContext";

export const login = async (user, dispatch) => {
    dispatch(loginStart)
    try{
        const resp = await axios.post("auth/login", user);
        if(resp.data.response) {
            toast.success(resp.data.message.text, {
                position: toast.POSITION.TOP_CENTER
            });
            dispatch(loginSuccess(resp.data.user))
        } else {
            toast.error(resp.data.message.text, {
                position: toast.POSITION.TOP_CENTER
            });
        }
    }catch (err) {
        dispatch(loginFailure)
    }
}

export const logout = async (dispatch) => {
    try{
        const resp = await axios.post("auth/logout", [],configHeaderJsonToken());
        if(resp.data.response) {
            dispatch(logoutSuccess());
        }
    }catch (err) {
        console.info(err)
        dispatch(loginFailure)
    }
}