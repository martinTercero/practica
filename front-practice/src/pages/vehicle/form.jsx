import React, {useState} from 'react';
import Form from 'react-bootstrap/Form';
import axios from "../../common/axiosInstance";
import {configHeaderJsonToken} from "../../context/authContext/AuthContext";

const FormVehicle = (props) => {

    const [catBrands, setCatBrands] = useState([]);

    React.useEffect(async () => {
        try {
            const resp = await axios.get("/cat-vehicle-brand/list-by-select",configHeaderJsonToken());
            if(resp.data.response)
                setCatBrands(resp.data.data)
        } catch (error) {
            console.log(error);
        }
    },[]);

    return (
        <div className="formVehicle">
            <div className="container">

                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Tipo </Form.Label>
                        <div className="col-md-9">
                            <Form.Check type="radio" required name="type" value="1" onChange={props.handleChange}
                                        checked={props.vehicle.type === "1"} label="Sedan" />
                            <Form.Check type="radio" required name="type" value="2"  onChange={props.handleChange}
                                        checked={props.vehicle.type === "2"} label="Motocicleta" />
                        </div>
                    </Form.Group>
                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Código </Form.Label>
                        <div className="col-md-9">
                        <Form.Control required maxlength={10} type="text" name={"code"} onChange={props.handleChange}  placeholder="Ingresa código" />
                        </div>
                    </Form.Group>

                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Nombre </Form.Label>
                        <div className="col-md-9">
                        <Form.Control type="text" required name={"name"} onChange={props.handleChange} placeholder="Ingresa nombre" />
                        </div>
                    </Form.Group>

                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Descripción detallada </Form.Label>
                        <div className="col-md-9">
                            <Form.Control name="det_description" onChange={props.handleChange}
                                as="textarea"
                                placeholder="..."
                                style={{ height: '100px' }}
                            />
                        </div>
                    </Form.Group>

                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Año Modelo </Form.Label>
                        <div className="col-md-9">
                            <Form.Control type="number" required  name={"year_model"} onChange={props.handleChange} placeholder="Ingresa año modelo" />
                        </div>
                    </Form.Group>

                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Motor </Form.Label>
                        <div className="col-md-9">
                        <Form.Control type="text" required  name={"engine"} onChange={props.handleChange} placeholder="Ingresa Motor" />
                        </div>
                    </Form.Group>
                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Serie </Form.Label>
                        <div className="col-md-9">
                        <Form.Control type="text"  required name={"serie"} onChange={props.handleChange} placeholder="Ingresa serie" />
                        </div>
                    </Form.Group>

                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">HP </Form.Label>
                        <div className="col-md-9">
                        <Form.Control type="number" required  name={"hp"} onChange={props.handleChange} placeholder="Ingresa Caballos de fuerza" />
                        </div>
                    </Form.Group>
                    <Form.Group className="mb-3 row" >
                        <Form.Label className="col-md-3">Marca </Form.Label>
                        <div className="col-md-9">
                            <Form.Select required  name={"brand"} onChange={props.handleChange}>
                                <option value={""}>Elige...</option>
                                {catBrands.map((item) => (
                                    <option key={item.id} value={item.id}>{item.name_full}</option>
                                ))}
                            </Form.Select>
                        </div>
                    </Form.Group>
            </div>
        </div>
    )
}

export default FormVehicle;