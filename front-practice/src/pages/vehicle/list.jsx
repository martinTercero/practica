import React, {useState} from 'react';
import Form from 'react-bootstrap/Form';
import {Table} from "react-bootstrap";

const ListVehicles = (props) => {

    const [filter, setFilter] = useState("");

    const handleFilterVehicleList = e => {
        setFilter(e.target.value)
        props.handleGetVehicleList(e.target.value)
    }

    return (
        <Table striped bordered hover>
            <thead>
            <tr>
                <th colSpan={6}>

                </th>
                <th>
                    <Form.Check type="radio" checked={filter==""} required name="typeFilter" value="" onChange={handleFilterVehicleList}
                                label="Todo" />
                </th>
                <th>
                    <Form.Check type="radio" checked={filter=="Sedan"} required name="typeFilter" value="Sedan" onChange={handleFilterVehicleList}
                                label="Sedan" />
                </th>
                <th>
                    <Form.Check type="radio" checked={filter=="Motocicleta"} required name="typeFilter" value="Motocicleta" onChange={handleFilterVehicleList}
                                label="Motocicleta" />
                </th>
            </tr>
            <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Detalle </th>
                <th>Motor</th>
                <th>Serie</th>
                <th>HP</th>
                <th>Tipo</th>
                <th>Marca</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {
                props.vehicleList.map((item)=>(
                    <tr key={item.id}>
                        <td>{item.code}</td>
                        <td>{item.name}</td>
                        <td>{item.det_description}</td>
                        <td>{item.engine}</td>
                        <td>{item.serie}</td>
                        <td>{item.hp}</td>
                        <td>{item.type}</td>
                        <td>{item.brand}</td>
                        <td>{item.active_f}</td>
                    </tr>
                ))
            }
            <tr>
            </tr>
            </tbody>
        </Table>
    )
}

export default ListVehicles;