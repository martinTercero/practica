import React, {useContext, useState} from 'react';
import {Container, NavLink, Table, Button, Modal, ModalBody, ModalFooter, ModalTitle } from "react-bootstrap";
import FormVehicle from "../vehicle/form";
import axios from "../../common/axiosInstance";
import {trackPromise} from "react-promise-tracker";
import ListVehicles from "../vehicle/list";
import Form from "react-bootstrap/Form";
import { toast } from 'react-toastify';
import {logout} from "../../context/authContext/apiCalls";
import {AuthContext, configHeaderJsonToken} from "../../context/authContext/AuthContext";

const Home = () => {
    const [showModal, setShowModal] = useState(false);
    const handleClose = () => setShowModal(false);
    const handleShow = () => setShowModal(true);

    const {dispatch} = useContext(AuthContext);
    const [vehicle, setVehicle] = useState({});
    const [vehicleList, setVehicleList] = useState([]);
    const [validated, setValidated] = useState(false);

    const handleChange = e => {
        setVehicle({
            ...vehicle,
            [e.target.name]: e.target.value,
        })
    }

    const vehicleSave = async () => {
        try {
            const resp = await axios.post("/cat-vehicle/save", vehicle,configHeaderJsonToken());
            if (resp.data.response) {
                setVehicleList(resp.data.data)
                toast.success(resp.data.message.text, {
                    position: toast.POSITION.TOP_CENTER
                });
                handleClose();
            } else {
                toast.error(resp.data.message.text, {
                    position: toast.POSITION.TOP_CENTER
                });
            }

        } catch (error) {
            console.log(error);
        }
    }

    const handleSave = (e) => {
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
            setValidated(true);
            return;
        }

        e.preventDefault();
        trackPromise(vehicleSave());
    }

    const getVehicleList = async (data) => {
        try {
            const resp = await axios.get(data!=null?"/cat-vehicle/list?filter="+data:"/cat-vehicle/list", configHeaderJsonToken());
            if(resp.data.response) {
                setVehicleList(resp.data.data)
                toast.success(resp.data.message.text, {
                    position: toast.POSITION.TOP_CENTER
                });
            } else {
                toast.success("Success Notification !", {
                    position: toast.POSITION.TOP_CENTER
                });
            }
        } catch (error) {
            console.log(error);
        }
    }

    const handleLogout = (e) => {
        e.preventDefault();
        trackPromise(logout(dispatch));
    }

    const handleGetVehicleList = (data=null) => {
        trackPromise(getVehicleList(data));
    }

    React.useEffect(async () => {
        try {
            const timer = setTimeout(() =>  handleGetVehicleList(), 3000);
            return () => clearTimeout(timer);
        } catch (error) {
            console.log(error);
        }
    },[]);

    return (
        <>
        <nav className="justify-content-end nav bg-dark"  >
            <div className="nav-item">
                <Button className="btn-outline-dark btn-danger" onClick={handleLogout} >Logout</Button>
            </div>
        </nav>
        <Container className="p-4">
            <div className={""}>
                <Button onClick={handleShow} className="btn mb-4" variant="primary">Agregar Vehículo </Button>
                <hr></hr>
                <ListVehicles handleGetVehicleList={handleGetVehicleList}  vehicleList={vehicleList}></ListVehicles>
            </div>
        </Container>

            <Modal show={showModal} onHide={handleClose}>
                <Form noValidate validated={validated} onSubmit={handleSave}>
                    <Modal.Header>
                        <ModalTitle>Vehículo</ModalTitle>
                    </Modal.Header>
                    <ModalBody>
                        <FormVehicle handleChange={handleChange} vehicle={vehicle}></FormVehicle>
                    </ModalBody>
                    <ModalFooter>
                        <Button variant="secondary" onClick={handleClose}>
                            Cancelar
                        </Button>
                        <Button variant="primary" type="submit">
                            Guardar
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
            </>
    )
}

export default Home;