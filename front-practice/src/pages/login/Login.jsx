import React, {useContext, useState} from 'react';
import {
    FormGroup,
    FormLabel,
    FormControl,
    FormText,
    FormCheck,
    Button,
    FloatingLabel,
    InputGroup,
    Row, Container
} from "react-bootstrap";
import "./login.scss";
import { login } from "../../context/authContext/apiCalls";
import {AuthContext} from "../../context/authContext/AuthContext";
import {trackPromise} from "react-promise-tracker";

const FormLogin = () => {

    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const {dispatch} = useContext(AuthContext);
    const handleLogin = (e) => {
        e.preventDefault();
        trackPromise(login({email,password},dispatch));
    }

    return (
        <div className="login">
            <div className="container">
                <form className="" onSubmit={handleLogin}>
                    <h2 className="text-primary">Log in</h2>
                    <FormGroup className="mb-3" >
                        <FloatingLabel controlId="floatingEmail" label="Email">
                            <FormControl type="email"
                                         onChange={(e) => setEmail(e.target.value)}
                                         placeholder="example@example.com"/>
                        </FloatingLabel>
                    </FormGroup>

                    <FormGroup className="mb-3" controlId="formBasicPassword">
                        <FloatingLabel controlId="floatingPassword" label="Password">
                            <FormControl type="password"
                                         onChange={(e) => setPassword(e.target.value)}
                                         placeholder="Password"/>
                        </FloatingLabel>
                    </FormGroup>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </form>
            </div>
        </div>
    )

}

export default FormLogin;
