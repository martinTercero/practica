import axios from 'axios'

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://api-v.fussacorp.com/api/v1',
});
export default instance;