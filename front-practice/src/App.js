import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import FormLogin from "./pages/login/Login";
import React, {useContext} from "react";
import {AuthContext} from "./context/authContext/AuthContext";
import Home from "./pages/home/Home";
import { usePromiseTracker } from "react-promise-tracker";
import {MutatingDots} from "react-loader-spinner";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    const {user} = useContext(AuthContext);
    const LoadingIndicator = props => {
        const { promiseInProgress } = usePromiseTracker();
        return (
            promiseInProgress &&
            <div style={{width: "100%",height: "200",display: "flex",justifyContent: "center",alignItems: "center",zIndex:9999}}>
            <MutatingDots
                heigth="100"
                width="100"
                color='grey'
                ariaLabel='loading'
            />
            </div>
          );
        }
  return (
    <div className="App">
        { user ? <Home></Home> : <FormLogin></FormLogin>}
        <LoadingIndicator/>
        <ToastContainer />
    </div>
  );

}

export default App;
