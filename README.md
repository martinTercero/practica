## Backend 

- Si se necesita probar el backend:
    - Ejecutar: 
        - php composer install 
    - Configurar archivo .env para conexión a base de datos.
    - Ejecutar:
        - php artisan migrate
        - php artisan db:seed

## Front

- Ejecutar: 
    - npm install 
- Ele endpoint esta configurado para hacer peticiones a:
    - https://api-v.fussacor.com
    La API la coloque en dicha url 
- ejecutar:
    - npm start
    - Al mostrarse la pantalla de login podrán iniciar sesión:
        - User: martin.galdino@gmail.com
        - Password: secret 